using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using SignalRChat.Auth;

namespace SignalRChat.Hubs
{
    public interface IChatClient
    {
        Task ReceiveMessage(string user, string message);
    }

    [Authorize]
    public class ChatHub : Hub<IChatClient>
    {
        private string SessionGroupName
        {
            get
            {
                string sessionId = Context.User.FindFirst(JwtConstants.SessionIdClaimKey).Value;
                return $"Session:{sessionId}:Users";
            }
        }

        public async Task SendMessage(string user, string message)
        {
            await Clients.Groups(SessionGroupName).ReceiveMessage(user, message);
        }

        public override async Task OnConnectedAsync()
        {
            string sessionId = Context.User.FindFirst(JwtConstants.SessionIdClaimKey).Value;
            string userId = Context.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await Groups.AddToGroupAsync(Context.ConnectionId, SessionGroupName);

            await base.OnConnectedAsync();
        }

        // Should probably implement disconnect maybe, if it's gonna do something with the database.
    }
}
