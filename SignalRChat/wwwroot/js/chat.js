"use strict";

document.getElementById('startButton').addEventListener('click', event => {
    sendInitRequest('start');
});

document.getElementById('joinButton').addEventListener('click', event => {
    sendInitRequest('join');
});

function sendInitRequest(url) {
    const user = document.getElementById("userInput").value;
    const session = document.getElementById('sessionNameInput').value;

    fetch(`/api/session/${url}`, {
        method: "POST",
        body: JSON.stringify({
            UserName: user,
            SessionName: session
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        console.log('init response recieved');
        console.log(`status: ${res.status}`);
        res.json().then(data => {
            console.log(data);
            const authToken = data.jwt;

            beginChat(authToken);
        });
    });
}

function beginChat(authToken) {
    document.getElementById('user-display').innerText = document.getElementById("userInput").value;
    document.getElementById('init-window').classList.add('d-none');
    document.getElementById('chat-window').classList.remove('d-none');

    var connection = new signalR.HubConnectionBuilder()
        .withUrl("/chatHub", {
            accessTokenFactory: () => authToken
        })
        .build();

    //Disable send button until connection is established
    document.getElementById("sendButton").disabled = true;

    connection.on("ReceiveMessage", function (user, message) {
        var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var encodedMsg = user + " says " + msg;
        var li = document.createElement("li");
        li.textContent = encodedMsg;
        document.getElementById("messagesList").appendChild(li);
    });

    connection.onclose(e => {
        console.log('connection closed unexpectedly');
        console.error(e);
    });

    connection.start().then(function () {
        document.getElementById("sendButton").disabled = false;
    }).catch(function (err) {
        return console.error(err.toString());
    });

    document.getElementById("sendButton").addEventListener("click", function (event) {
        var user = document.getElementById("userInput").value;
        var message = document.getElementById("messageInput").value;
        connection.invoke("SendMessage", user, message).catch(function (err) {
            return console.error(err.toString());
        });
        event.preventDefault();
    });
}