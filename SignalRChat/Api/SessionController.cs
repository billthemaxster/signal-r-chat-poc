﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SignalRChat.Auth;
using SignalRChat.Database;

namespace SignalRChat.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly IDatabase _database;
        private readonly IJwtProvider _jwtProvider;

        public SessionController(
            IDatabase database,
            IJwtProvider jwtProvider)
        {
            _database = database;
            _jwtProvider = jwtProvider;
        }

        public record CreateModel(string SessionName, string UserName);

        [HttpPost("start")]
        public IActionResult Start(CreateModel model)
        {
            Session session = new()
            {
                Name = model.SessionName,
                Users = new List<string>
                {
                    model.UserName
                }
            };

            _database.CreateSession(session);

            string authToken = _jwtProvider.GenerateToken(model.SessionName, model.UserName);

            return Ok(new
            {
                Jwt = authToken
            });
        }

        public record JoinModel(string SessionName, string UserName);

        [HttpPost("join")]
        public IActionResult Join(JoinModel model)
        {
            _database.AddUser(model.SessionName, model.UserName);

            string authToken = _jwtProvider.GenerateToken(model.SessionName, model.UserName);

            return Ok(new
            {
                Jwt = authToken
            });
        }
    }
}
