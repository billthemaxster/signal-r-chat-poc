﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace SignalRChat.Auth
{
    public interface IJwtProvider
    {
        string GenerateToken(string sessionName, string userName);
    }

    public class JwtProvider : IJwtProvider
    {
        private readonly JwtConfig _config;

        public JwtProvider(IOptions<JwtConfig> opt)
        {
            _config = opt.Value;
        }

        public string GenerateToken(string sessionName, string userName)
        {
            SymmetricSecurityKey signingKey = new(Encoding.UTF8.GetBytes(_config.SigningKey));

            JwtSecurityTokenHandler handler = new();
            SecurityTokenDescriptor descriptor = new()
            {
                Subject = new(new Claim[]
                {
                    new(ClaimTypes.NameIdentifier, userName),
                    new(JwtConstants.SessionIdClaimKey, sessionName)
                }),
                Expires = DateTime.UtcNow.AddMinutes(60),
                Issuer = _config.Issuer,
                Audience = _config.Audience,
                SigningCredentials = new(signingKey, SecurityAlgorithms.HmacSha512)
            };

            SecurityToken token = handler.CreateToken(descriptor);

            return handler.WriteToken(token);
        }
    }
}
