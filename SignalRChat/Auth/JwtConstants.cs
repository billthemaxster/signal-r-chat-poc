﻿namespace SignalRChat.Auth
{
    public static class JwtConstants
    {
        public const string SessionIdClaimKey = "SessionId";
    }
}
