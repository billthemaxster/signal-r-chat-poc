﻿namespace SignalRChat.Auth
{
    public record JwtConfig
    {
        public string SigningKey { get; init; }
        public string Issuer { get; init; }
        public string Audience { get; init; }
    }
}
