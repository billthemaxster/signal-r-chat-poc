﻿using System.Collections.Generic;

namespace SignalRChat.Database
{
    public class Session
    {
        public string Name { get; set; }

        public List<string> Users { get; set; } = new();

        public Dictionary<string, string> UserConnections { get; set; } = new();
    }
}
