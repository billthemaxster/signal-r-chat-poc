﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace SignalRChat.Database
{
    /// <summary>
    /// Super basic in-memory db for POC.
    /// </summary>
    public interface IDatabase
    {
        void CreateSession(Session session);

        void AddUser(string sessionName, string userName);

        void RemoveUser(string sessionName, string userName);

        void DeleteSession(string sessionName);

        Session GetSessionById(string sessionName);

        void ConnectUser(string sessionName, string userName, string connectionId);
    }

    public class InMemoryDatabase : IDatabase
    {
        private readonly ILogger<InMemoryDatabase> _logger;
        private readonly Dictionary<string, Session> _sessions = new();

        public InMemoryDatabase(ILogger<InMemoryDatabase> logger)
        {
            _logger = logger;
        }

        public void CreateSession(Session session)
        {
            _sessions.Add(session.Name, session);
            _logger.LogInformation($"Database: {JsonSerializer.Serialize(session)}");
        }

        public void DeleteSession(string sessionName)
        {
            _sessions.Remove(sessionName);
        }

        public Session GetSessionById(string sessionName)
        {
            return _sessions[sessionName];
        }

        public void AddUser(string sessionName, string userName)
        {
            var session = _sessions[sessionName];

            if (!session.Users.Contains(userName))
            {
                session.Users.Add(userName);
                _logger.LogInformation($"Database: {JsonSerializer.Serialize(session)}");
            }
            else
            {
                throw new InvalidOperationException("Cannot add a user when it already exists");
            }
        }

        public void RemoveUser(string sessionName, string userName)
        {
            var session = _sessions[sessionName];

            session.Users.Remove(userName);
            session.UserConnections.Remove(userName);
        }

        // This method (or something of it's ilk) might be useful for implementing 
        // that only a single user with an access token can be permitted at once.
        public void ConnectUser(string sessionName, string userName, string connectionId)
        {
            var session = _sessions[sessionName];

            if (session.Users.Contains(userName))
            {
                session.UserConnections[userName] = connectionId;

                _logger.LogInformation($"Database: {JsonSerializer.Serialize(session)}");
            }
            else
            {
                throw new InvalidOperationException("User does not exist!");
            }
        }
    }
}
