﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using ChatClient;

Console.WriteLine("Welcome to Simple Chat");
Console.WriteLine("Enter 'start' to start a new session or 'join' to join an existing session");
string input = Console.ReadLine().ToLowerInvariant();

await ParseInput(input);

async Task ParseInput(string input)
{
    while (true)
    {
        switch (input)
        {
            case "join":
                await Join();
                return;

            case "start":
                await Start();
                return;

            default:
                Console.WriteLine("Invalid input enter 'start' or 'join'");
                input = Console.ReadLine();
                break;
        }
    }
}

async Task Join()
{
    Console.WriteLine("You've selected to join a session.");
    Console.WriteLine();

    (string sessionName, string userName) = GetChatSessionInfo();
    string authToken = await SendAuthRequest("api/session/join", sessionName, userName);

    await ChatSession.StartSession(authToken, userName);
}

async Task Start()
{
    Console.WriteLine("You've selected to start a session.");
    Console.WriteLine();

    (string sessionName, string userName) = GetChatSessionInfo();
    string authToken = await SendAuthRequest("api/session/start", sessionName, userName);

    await ChatSession.StartSession(authToken, userName);
}

async Task<string> SendAuthRequest(string url, string sessionName, string userName)
{

    HttpClient httpClient = new();
    HttpRequestMessage request = new(HttpMethod.Post, $"https://localhost:5001/{url}")
    {
        Content = new StringContent(JsonSerializer.Serialize(new
        {
            SessionName = sessionName,
            UserName = userName
        }),
        Encoding.UTF8,
        "application/json")
    };

    HttpResponseMessage response = await httpClient.SendAsync(request);
    string body = await response.Content.ReadAsStringAsync();
    StartJoinResponseModel data = JsonSerializer.Deserialize<StartJoinResponseModel>(body, new() { PropertyNameCaseInsensitive = true });

    return data.Jwt;
}

(string sessionName, string userName) GetChatSessionInfo()
{
    Console.WriteLine("Please enter a session name:");
    string sessionName = Console.ReadLine();

    Console.WriteLine("Please enter your name");
    string userName = Console.ReadLine();
    return (sessionName, userName);
}

record StartJoinResponseModel(string Jwt);
