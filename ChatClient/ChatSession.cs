﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;

namespace ChatClient
{
    public static class ChatSession
    {
        public static async Task StartSession(string authKey, string userName)
        {
            HubConnection connection = new HubConnectionBuilder()
                            .WithUrl(
                                "http://localhost:5000/chathub",
                                opt =>
                                {
                                    opt.AccessTokenProvider = () =>
                                    {
                                        string accessToken = $"Bearer {authKey}";
                                        Console.WriteLine($"Getting access token: {accessToken}");
                                        return Task.FromResult(accessToken);
                                    };
                                    //opt.Transports = HttpTransportType.LongPolling;
                                })
                            
                            .ConfigureLogging(logging =>
                            {
                                //logging.SetMinimumLevel(LogLevel.Debug);
                                logging.AddConsole();
                            })
                            .Build();

            await connection.StartAsync();
            
            Console.WriteLine("Starting connection. Press Ctrl-C to close.");

            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (sender, a) =>
            {
                a.Cancel = true;
                cts.Cancel();
            };

            connection.Closed += e =>
            {
                Console.WriteLine("Connection closed with error: {0}", e);

                cts.Cancel();
                return Task.CompletedTask;
            };

            connection.On("ReceiveMessage", (string user, string message) =>
            {
                Console.WriteLine($"{user}: {message}");
            });

            while (!cts.IsCancellationRequested)
            {
                string message = Console.ReadLine();
                await connection.SendAsync("SendMessage", userName, message, cts.Token);
            }
        }
    }
}
